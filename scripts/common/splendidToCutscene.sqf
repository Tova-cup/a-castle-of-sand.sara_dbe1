while {true} do {
	waitUntil {
		uiSleep 1;
		not isNull findDisplay 314;
	};

	findDisplay 314 displayAddEventHandler ["KeyUp", {
		params ["_displayorcontrol", "_key", "_shift", "_ctrl", "_alt"];
		
		if (_key isEqualTo 46 && _ctrl) exitWith {
			_camParameters=call compile copyFromClipboard;
			_targetPos=screenToWorld [0.5, 0.5];

			_camOutput = Format ["
%1 camPrepareTarget %2;
%1 camPreparePos %3;
%1 camPrepareFOV %4;
//setAperture %5;
//%1 setDir %8;
//[%1, %6, %7] call BIS_fnc_setPitchBank;
%1 camCommitPrepared 0;
//waitUntil {camCommitted %1};
",
"_camera",_targetPos,_camParameters select 1,_camParameters select 3,_camParameters select 6,_camParameters select 4 select 0,_camParameters select 4 select 1,_camParameters select 2
			];

			copyToClipboard _camOutput;
		};
	}];

	waitUntil {
		uiSleep 1;
		isNull findDisplay 314;
	};

}