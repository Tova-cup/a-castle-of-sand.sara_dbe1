//Move and stay, to move subSquads somewhere, used for scripted bounding, myUnits must be an array

params ["_myUnits","_destination",["_finalBehaviour","AWARE"],["_finalStance","AUTO"]];
_myUnits=_myUnits-[player];
_safePosArray=[_destination, 0, 2, 0, 0];
_initialGroup=group (_myUnits select 0);
_tmpGroup=createGroup [side (_myUnits select 0), false];  
{
	_x disableAI "AUTOCOMBAT";
	_x setUnitPos "AUTO";
	[_x] joinSilent _tmpGroup;
	_x setBehaviour "AWARE";
	[_x] joinSilent _initialGroup;
	_x doMove (_safePosArray call BIS_fnc_findSafePos);
} forEach _myUnits;

sleep 1;
_stillMovingUnits=_myUnits;
while {sleep 0.5; count _stillMovingUnits>0 } do {
	//hintSilent str _stillMovingUnits;
	{
		if (unitReady _x) then {
			doStop _x;
			[_x] joinSilent _tmpGroup;
			_x setUnitPos _finalStance;
			_x setBehaviour _finalBehaviour;
			[_x] joinSilent _initialGroup;
			_x enableAI "AUTOCOMBAT";
			_stillMovingUnits=_stillMovingUnits-[_x];
			doStop _x;
			};
	} forEach _stillMovingUnits;
};
deleteGroup _tmpGroup;
//hint "over";
