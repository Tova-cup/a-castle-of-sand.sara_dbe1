params["_target","_text","_value","_fsmHandle"];

_actionId=_target addAction [
	"<t size='2'>"+_text+"</t>",
	{
		params ["_target", "_caller", "_actionId", "_arguments"];
		_arguments params ["_value","_fsmHandle"];
		_target removeAction _actionId;
		//hint _fsmHandle;
		[_fsmHandle,["_next", _value]]remoteExec ["setFSMVariable",2];
		[_fsmHandle,["_whoInteracted", _caller]]remoteExec ["setFSMVariable",2];
	},
	[_value,_fsmHandle],
	1.5,
	true,
	true,
	"",
	"_this distance _target < 3 && (isNull TOV_Who_Speaking) && alive _target && !(_target getVariable ['ACE_isUnconscious', false]) && !(_target getVariable ['_isBeingTalkedTo',false])",
	3
];

/*	
_choices=player getVariable [format["_TOV_choices%1",_fsmHandle], []];
_choices pushBack _actionId;
//hint str _choices;	
player setVariable [format["_TOV_choices%1",_fsmHandle],_choices,true];
*/