//script to add a talk action to a unit.
//script is local
//_this can be used in talkcode
//_talkCode must be in format {...Your code...}
//params ["_target", "_caller", "_actionId", "_arguments"]; are the params given to the code as _this

params ["_target","_talkCode",["_removeOnCompletion",true],["_clearScreen",true],["_otherArguments",[]]];

[
	_target,											// Object the action is attached to
	"Talk",										// Title of the action
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",	// Idle icon shown on screen
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",	// Progress icon shown on screen
	"_this distance _target < 3 && (isNull TOV_Who_Speaking) && alive _target && !(_target getVariable ['ACE_isUnconscious', false]) && !(_target getVariable ['_isBeingTalkedTo',false])",		// Condition for the action to be shown
	"_caller distance _target < 3",						// Condition for the action to progress
	{},													// Code executed when action starts
	{},													// Code executed on every progress tick
	{
		params ["_target", "_caller", "_actionId", "_arguments"];
		_target setVariable ["_isBeingTalkedTo",true,true];
		_arguments params ["_talkCode","_clearScreen","_otherArguments"];
		_handle=_this spawn _talkCode;
		waitUntil {scriptDone _handle};
		if (_clearScreen) then {
			[_target,"","DESCRIPTION",false,true,0.2] call TOV_Talk;
		};
		_target setVariable ["_isBeingTalkedTo",false,true];
	},													// Code executed on completion
	{},													// Code executed on interrupted
	[_talkCode,_clearScreen,_otherArguments],										// Arguments passed to the scripts as _this select 3
	1,													// Action duration [s]
	100,													// Priority
	_removeOnCompletion,												// Remove on completion
	false												// Show in unconscious state 
] call BIS_fnc_holdActionAdd;							// MP compatible implementation