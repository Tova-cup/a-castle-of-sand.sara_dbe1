
// filter is text, layer is text
params [["_layer",""],["_filter",""],["_deleteAfterGrab",false]];
if (_layer isEqualTo "") exitWith {systemchat "You didn't give a layer name !"};

_wpMarkers = [];

{
	if ((toUpper (str _x) find toUpper _filter >= 0) || _filter isEqualTo "")  then {_wpMarkers pushback _x};
} forEach (getMissionLayerEntities _layer select 0);

_wpMarkers=[_wpMarkers, [], {parseNumber ((str _x splitString "_") select 1)}, "ASCEND"] call BIS_fnc_sortBy;
if (_deleteAfterGrab) then {{deleteVehicle  _x} forEach _wpMarkers;};
_wpMarkers apply {getposATL _x};
