params ["_classname", "_position",["_direction",0],["_noMove",false],["_anim",""]];

_dude=0;
if (_classname isKindOf "Man") then {
	_dude=createAgent [_classname, _position, [], 0, 'CAN_COLLIDE'];
} else {
	_dude=createVehicle [_classname, _position, [], 0, 'CAN_COLLIDE'];
};
_dude enableSimulation false;
_dude setcaptive true;
_dude setBehaviour 'CARELESS'; 
_dude disableAI 'FSM';

if (_noMove) then {_dude disableAI 'MOVE';};
if !(_anim isEqualTo "" ) then { _dude disableAI "ANIM"; _dude switchMove _anim};

_dude setposATL _position;
_dude setDir _direction;
if (_classname isKindOf "Man") then { _dude enableSimulation true;};
_dude;