/* 
CORE FUNCTIONS FOR TPW MODS
Author: tpw 
Date: 20190928
Version: 1.77
Requires: CBA A3
Compatibility: N/A

Disclaimer: Feel free to use and modify this code, on the proviso that you post back changes and improvements so that everyone can benefit from them, and acknowledge the original author (tpw) in any derivative works.     

To use: 
1 - Save this script into your mission directory as eg tpw_core.sqf
2 - Call it with 0 = [["C_MAN","C_MAN","CUP_C","CAF_AG","CAF_AG","C_MAN"],["str1","str2",etc],5,23] execvm "tpw_core.sqf"; // where  ["C_MAN","C_MAN","CUP_C","CAF_AG","CAF_AG","C_MAN"] = civilian classname strings for Mediterranean, Oceanian, European, Mideastern, African, Asian maps respectively, ["str1","str2",etc] civs containing these strings in their classnames will be excluded, 5 = reduced civilian ambience will be spawned before this time (-1 to disable), 23 = reduced civilian ambience will be spawned after this time (-1 to disable). 

TPW MODS WILL NOT FUNCTION WITHOUT THIS SCRIPT RUNNING
*/
if (isDedicated) exitWith {};
if (count _this < 4) exitwith {player sidechat "TPW CORE incorrect/no config, exiting."};

// VARS
tpw_core_mapstrings = _this select 0; // Strings to select per map civilians
tpw_core_blacklist = _this select 1; // Civilians to exclude
tpw_core_morning = _this select 2; // Reduced ambience before this time. Set to -1 to disable
tpw_core_night = _this select 3; // Reduced ambience after this time. Set to 25 to disable
tpw_core_active = true;
tpw_mods_version = "20190928"; // will appear on start hint

// MAP SPECIFIC MOD DISABLING
//Maps without roads - no cars
if (worldname in  ["pja307","pja319"]) then
	{
	tpw_car_active = false;
	tpw_park_active = false;
	};

// No aircraft
if (worldname in ["mak_Jungle","isladuala"]) then
	{
	tpw_air_active = false;
	};

// Winter 2035 specific
if (tolower worldname in ["stratis","altis"] && isclass (configfile/"CfgPatches"/"winter_2035")) then
	{
	tpw_winter2035 = true;
	} else
	{
	tpw_winter2035 = false;
	};
	
// CLOTHES
tpw_core_trouserlist = [
"U_Competitor",
"U_C_HunterBody_grn",
"U_C_Poor_1",
"U_C_Poor_2",
"U_IG_Guerilla2_2",
"U_IG_Guerilla2_3",
"U_IG_Guerilla3_1",
"U_IG_Guerilla3_2",
"U_I_G_resistanceLeader_F",
"U_I_L_Uniform_01_tshirt_olive_F",
"U_NikosBody",
"U_Marshal",
"U_C_Journalist",
"U_Rangemaster",
"U_C_Mechanic_01_F",
"U_NikosAgedBody",
// Tanoa
"u_c_man_casual_1_f",
"u_c_man_casual_2_f",
"u_c_man_casual_3_f",
"u_i_c_soldier_bandit_2_f",
"u_i_c_soldier_bandit_3_f",
"U_B_GEN_Commander_F",
//IDAP
"U_C_IDAP_Man_cargo_F",
"U_C_IDAP_Man_casual_F",
"U_C_IDAP_Man_Jeans_F",
"U_C_IDAP_Man_Tee_F",
// Livonia
"U_I_L_Uniform_01_camo_F",
"U_I_L_Uniform_01_deserter_F",
"U_I_L_Uniform_01_tshirt_black_F",
"U_I_L_Uniform_01_tshirt_olive_F",
"U_I_L_Uniform_01_tshirt_skull_F",
"U_I_L_Uniform_01_tshirt_sport_F",
"U_C_Uniform_Scientist_01_F",
"U_C_Uniform_Scientist_01_formal_F",
"U_C_Uniform_Scientist_02_formal_F",
"U_C_Uniform_Farmer_01_F",
"U_C_E_LooterJacket_01_F",
"U_O_R_Gorka_01_black_F",
// Weferlingen
"gm_gc_civ_uniform_man_01_80_blk",
"gm_gc_civ_uniform_man_01_80_blu",
"gm_gc_civ_uniform_man_02_80_brn",
// EO Gear
"eo_hoodie_bandit",
"eo_hoodie_kabeiroi",
"eo_hoodie_red",
"eo_hoodie_grey",
"eo_hoodie_blue",
"eo_retro_bandit",
"eo_retro_kabeiroi",
"eo_retro_red",
"eo_retro_grey",
"eo_retro_green",
"eo_bandit_1",
"eo_diamond_1",
"eo_survivor_1",
"eo_independant_1",
"eo_shirt_check",
"eo_shirt_stripe",
"eo_shirt_bandit",
"eo_shirt_kabeiroi"
];

tpw_core_shortlist = [
"U_C_Poloshirt_blue",
"U_C_Poloshirt_burgundy",
"U_C_Poloshirt_redwhite",
"U_C_Poloshirt_salmon",
"U_C_Poloshirt_stripped",
"U_C_Poloshirt_tricolour",
// Tanoa
"u_c_man_casual_4_f",
"u_c_man_casual_5_f",
"u_c_man_casual_6_f",
"u_i_c_soldier_bandit_1_f",
"u_i_c_soldier_bandit_4_f",
"u_i_c_soldier_bandit_5_f",
// IDAP
"U_C_IDAP_Man_shorts_F",
"U_C_IDAP_Man_TeeShorts_F",
// Livonia
"U_C_Uniform_Scientist_02_F"
];

tpw_core_hats = 
["H_Hat_Safari_sand_F",
"H_Booniehat_khk",
"H_Booniehat_oli",
"H_Booniehat_indp",
"H_Booniehat_mcamo",
"H_Booniehat_grn",
"H_Booniehat_tan",
"H_Booniehat_dirty",
"H_Booniehat_dgtl",
"H_Booniehat_khk_hs",
"H_Cap_red",
"H_Cap_blu",
"H_Cap_oli",
"H_Cap_headphones",
"H_Cap_tan",
"H_Cap_blk",
"H_Cap_blk_CMMG",
"H_Cap_brn_SPECOPS",
"H_Cap_tan_specops_US",
"H_Cap_khaki_specops_UK",
"H_Cap_grn",
"H_Cap_grn_BI",
"H_Cap_blk_Raven",
"H_Cap_blk_ION",
"H_Cap_oli_hs",
"H_Cap_press",
"H_Cap_usblack",
"H_Cap_surfer",
"H_Cap_police",
"H_Bandanna_surfer",
"H_Bandanna_khk",
"H_Bandanna_khk_hs",
"H_Bandanna_cbr",
"H_Bandanna_sgg",
"H_Bandanna_sand",
"H_Bandanna_surfer_blk",
"H_Bandanna_surfer_grn",
"H_Bandanna_gry",
"H_Bandanna_blu",
"H_Beret_red",
"H_Beret_grn",
"H_Beret_grn_SF",
"H_Beret_brn_SF",
"H_Beret_ocamo",
"H_Beret_02",
"H_Beret_Colonel",
"H_Watchcap_blk",
"H_Watchcap_cbr",
"H_Watchcap_khk",
"H_Watchcap_camo",
"H_Watchcap_sgg",
"H_StrawHat",
"H_StrawHat_dark",
"H_Hat_blue",
"H_Hat_camo",
"H_Hat_grey",
"H_Hat_checker",
"H_Hat_tan",
"H_Cap_oli_Syndikat_F",
"H_Cap_tan_Syndikat_F",
"H_Cap_blk_Syndikat_F",
"H_Cap_grn_Syndikat_F",
"H_Hat_Safari_olive_F",
"H_Cap_White_IDAP_F",
"H_Cap_Orange_IDAP_F",
"H_Cap_Black_IDAP_F",
"H_Booniehat_mgrn",
"H_Booniehat_taiga",
"H_Booniehat_eaf",
"H_Booniehat_wdl",
"eo_hat_brown",
"eo_hat_gray",
"eo_hat_tan"
];

// SCAN AVAILABLE CLOTHES
tpw_core_trousers = [];
tpw_core_shorts = [];
	{
	if (isclass (configfile/"CfgWeapons"/_x)) then 
		{
		tpw_core_trousers pushback _x;
		};
	} foreach tpw_core_trouserlist;
	
	{
	if (isclass (configfile/"CfgWeapons"/_x)) then 
		{
		tpw_core_shorts pushback _x;
		};
	} foreach tpw_core_shortlist;
	

// ADD ARG CLOTHING IF AVAILABLE
private ["_cfg","_str"];
_cfg = (configFile >> "CfgWeapons");
_str = _this select 0;
for "_i" from 0 to ((count _cfg) -1) do 
	{
	if (isClass ((_cfg select _i)) ) then 
		{
		_cfgName = configName (_cfg select _i);
		if (["arg_shirt",str _cfgname] call BIS_fnc_inString &&{random 1  < 0.4}) then 
			{
			tpw_core_trousers set [count tpw_core_trousers,_cfgname];
			};
		};
	};	

// Screen out uniforms based on exclusion strings
for "_i" from 0 to (count tpw_core_trousers - 1) do	
	{	
	_item = tpw_core_trousers select _i;
		{
		if ([_x,str _item] call BIS_fnc_inString) then
			{
			tpw_core_trousers set [_i, -1];
			};
		} foreach tpw_core_blacklist;
	};
tpw_core_trousers = tpw_core_trousers - [-1];

for "_i" from 0 to (count tpw_core_shorts - 1) do	
	{	
	_item = tpw_core_shorts select _i;
		{
		if ([_x,str _item] call BIS_fnc_inString) then
			{
			tpw_core_shorts set [_i, -1];
			};
		} foreach tpw_core_blacklist;
	};
tpw_core_shorts = tpw_core_shorts - [-1];

for "_i" from 0 to (count tpw_core_hats - 1) do	
	{	
	_item = tpw_core_hats select _i;
		{
		if ([_x,str _item] call BIS_fnc_inString) then
			{
			tpw_core_hats set [_i, -1];
			};
		} foreach tpw_core_blacklist;
	};
tpw_core_hats = tpw_core_hats - [-1];

	

tpw_core_fnc_clothes =
	{
	private ["_clothes"];
	
	// Appropriate clothing for climate
	if (true) then
		{
		_clothes = tpw_core_trousers + tpw_core_shorts + tpw_core_shorts;
		} else
		{
		_clothes = tpw_core_trousers;
		};
	_clothes
	};

// GRAB CIVS FROM CONFIG
tpw_core_fnc_grabciv =
	{
	private ["_cfg","_str"];
	tpw_core_civs = [];
	_cfg = (configFile >> "CfgVehicles");
	_str = _this select 0;
	for "_i" from 0 to ((count _cfg) -1) do 
		{
		if (isClass ((_cfg select _i) ) ) then 
			{
			_cfgName = configName (_cfg select _i);
			if ( (_cfgName isKindOf "camanbase") && {getNumber ((_cfg select _i) >> "scope") == 2} && {[_str,str _cfgname] call BIS_fnc_inString}) then 
				{
				tpw_core_civs set [count tpw_core_civs,_cfgname];
				};
			};
		};		
		
	// Use default BIS civs if custom string can't be found
	if (count tpw_core_civs < 2)then 
		{
		_str = "c_man";
		for "_i" from 0 to ((count _cfg) -1) do 
			{
			if (isClass ((_cfg select _i) ) ) then 
				{
				_cfgName = configName (_cfg select _i);
				if ( (_cfgName isKindOf "camanbase") && {getNumber ((_cfg select _i) >> "scope") == 2} && {[_str,str _cfgname] call BIS_fnc_inString}) then 
					{
					tpw_core_civs set [count tpw_core_civs,_cfgname];
					};
				};
			};	
		};	
	_str
	};
	
// GRAB FEMALE CIVS FROM CONFIG
tpw_core_fnc_grabfemciv =
	{
	private ["_cfg","_str"];
	_cfg = (configFile >> "CfgVehicles");
	_str = "Max_woman";
	for "_i" from 0 to ((count _cfg) -1) do 
		{
		if (isClass ((_cfg select _i) ) ) then 
			{
			_cfgName = configName (_cfg select _i);
			if ( (_cfgName isKindOf "camanbase") && {getNumber ((_cfg select _i) >> "scope") == 2} && {[_str,str _cfgname] call BIS_fnc_inString} && {!(["soldier",str _cfgname] call BIS_fnc_inString)}) then 
				{
				tpw_core_civs set [count tpw_core_civs,_cfgname];
				tpw_core_civs set [count tpw_core_civs,_cfgname];
				};
			};
		};
	};

// GRAB IDAP CIVS FROM CONFIG
tpw_core_fnc_grabidapciv =
	{
	private ["_cfg","_str"];
	_cfg = (configFile >> "CfgVehicles");
	_str = "IDAP";
	for "_i" from 0 to ((count _cfg) -1) do 
		{
		if (isClass ((_cfg select _i) ) ) then 
			{
			_cfgName = configName (_cfg select _i);
			if ( (_cfgName isKindOf "camanbase") && {getNumber ((_cfg select _i) >> "scope") == 2} && {[_str,str _cfgname] call BIS_fnc_inString} && {!(["soldier",str _cfgname] call BIS_fnc_inString)}) then 
				{
				tpw_core_civs set [count tpw_core_civs,_cfgname];
				};
			};
		};
	};		

// REGION SPECIFIC CIVILIANS
tpw_core_fnc_civs =
	{
	private ["_civstring","_mideast","_african","_asian","_ethnicity","_ext"];
	private _civstrings = _this select 0;
	
	tpw_core_european = [	
		"bush_island_51",
		"carraigdubh",
		"chernarus",
		"chernarus_summer",
		"chernarusredux",
		"fdf_isle1_a",
		"mbg_celle2",
		"woodland_acr",
		"bootcamp_acr",
		"thirsk",
		"thirskw",
		"utes",
		"gsep_mosch",
		"gsep_zernovo",
		"bornholm",
		"anim_helvantis_v2",
		"wgl_palms",
		"colleville",
		"staszow",
		"baranow",
		"panovo",
		"ivachev",
		"xcam_taunus",
		"abramia",
		"napfwinter",
		"beketov",
		"chernarus_winter",
		"utes_winter",
		"thirskw",
		"arctic",
		"hellanmaa",
		"blud_vidda",
		"ruha",
		"sennoe",
		"chernarus_2035",
		"kapaulio",
		"vis",
		"atmt_trava",
		"wl_rosche",
		"tem_summa",
		"gm_weferlingen_summer",
		"gm_weferlingen_winter",
		"enoch",
		"tem_cham",
		"tem_chamw",
		"tem_suursaariv",
		"tem_chernarus",
		"tem_ihantala"
		];

	tpw_core_greek = [
		"stratis",
		"altis",
		"imrali",
		"pja314",
		"malden",
		"bozcaada",
		"tembelan",
		"porquerolles",
		"hebontes",
		"pianosa_aut",
		"montellav3"
		];
	
	tpw_core_mideast = [
		"mcn_aliabad",
		"mcn_hazarkot",
		"bmfayshkhabur",
		"clafghan",
		"fallujah",
		"fata",
		"hellskitchen",
		"hellskitchens",
		"mcn_hazarkot",
		"praa_av",
		"reshmaan",
		"shapur_baf",
		"takistan",
		"torabora",
		"tup_qom",
		"zargabad",
		"pja307",
		"pja306",
		"pja308",
		"pja310",
		"mountains_acr",
		"tunba",
		"kunduz",
		"mog",
		"waziristan",
		"dya",
		"lythium",
		"pja319",
		"huntersvalley",
		"farkhar",
		"altiplano",
		"mog",
		"tem_anizay",
		"cup_kunduz",
		"khoramshahr"
		];
	
	tpw_core_african = [
		"mak_jungle",
		"pja305",
		"tropica",
		"tigeria",
		"tigeria_se",
		"sara",
		"saralite",
		"sara_dbe1",
		"porto",
		"intro",
		"kidal",
		"isladuala3",
		"bsoc_brasil",
		"lingor3",
		"dingor",
		"seanglola",
		"sfp_wamako",
		"tem_kujari"
		];
		
	tpw_core_asian = [
		"pja312",
		"prei_khmaoch_luong",
		 "us101_cao_bang",
		 "dakrong",
		 "uns_dong_ha",
		 "rungsat",
		 "csj_sea",
		 "csj_lowlands",
		 "phu_bai",
		 "uns_ptv",
		 "rockwall"
		];
		
	tpw_core_oceania = [
		"tanoa",
		"pulau",
		"126map"
	];	
	
	// Ethnicity based on worldname
	_ethnicity = "default"; // Default
	if (tolower worldname in tpw_core_european) then {_ethnicity = "european"};
	if (tolower worldname in tpw_core_greek) then {_ethnicity = "greek"};
	if (tolower worldname in tpw_core_mideast) then {_ethnicity = "mideast"};
	if (tolower worldname in tpw_core_african) then {_ethnicity = "african"};
	if (tolower worldname in tpw_core_asian) then {_ethnicity = "asian"};
	if (tolower worldname in tpw_core_oceania) then {_ethnicity = "oceania"};
	
	// Greeks (eg Altis/Stratis)
	if (_ethnicity == "greek") then 
		{
		_civstring = [_civstrings select 0] call tpw_core_fnc_grabciv;
	
		// Screen out non-Greeks from BIS civs
		if (_civstring == "c_man") then
			{
			for "_i" from 0 to (count tpw_core_civs - 1) do	
				{	
				_civ = tpw_core_civs select _i;
				if ((["unarmed",str _civ] call BIS_fnc_inString)  || (["euro",str _civ] call BIS_fnc_inString) || (["asia",str _civ] call BIS_fnc_inString)||(["afro",str _civ] call BIS_fnc_inString)||(["tanoa",str _civ] call BIS_fnc_inString)) then
					{
					tpw_core_civs set [_i, -1];
					};
				};
			tpw_core_civs = tpw_core_civs - [-1];
			};
		// Females, IDAP
		[] call tpw_core_fnc_grabfemciv;	
		//[] call	tpw_core_fnc_grabidapciv;
		};	
	
	// Oceania (eg Tanoa)
	if (_ethnicity == "oceania") then 
		{
		_civstring = [_civstrings select 1] call tpw_core_fnc_grabciv;
	
		// Screen out non-Tanoans from BIS civs
		if (_civstring == "c_man") then
			{
			for "_i" from 0 to (count tpw_core_civs - 1) do	
				{	
				_civ = tpw_core_civs select _i;
				if !(["tanoa",str _civ] call BIS_fnc_inString) then
					{
					tpw_core_civs set [_i, -1];
					};
				};
			tpw_core_civs = tpw_core_civs - [-1];	
			};
		// IDAP
		//[] call	tpw_core_fnc_grabidapciv;	
		};	
	
	// Europeans (eg Chernarus)
	if (_ethnicity == "european") then 
		{
		_civstring = [_civstrings select 2] call tpw_core_fnc_grabciv;
	
		// Screen out non-Europeans from BIS civs
		if (_civstring == "c_man") then
			{
			for "_i" from 0 to (count tpw_core_civs - 1) do	
				{	
				_civ = tpw_core_civs select _i;
				if !(["euro",str _civ] call BIS_fnc_inString) then
					{
					tpw_core_civs set [_i, -1];
					};
				};
			tpw_core_civs = tpw_core_civs - [-1];		
			};

		// Females, IDAP
		[] call tpw_core_fnc_grabfemciv;	
		//[] call	tpw_core_fnc_grabidapciv;
		};
		
	// Mid East (eg Takistan)
	if (_ethnicity == "mideast") then 
		{
		_civstring = [_civstrings select 3] call tpw_core_fnc_grabciv;
	
		// Use Persian soldiers as civs
		if (_civstring == "c_man") then
			{
			tpw_core_civs = ["o_soldier_f"];	
			};
		};

	// Africans (eg N'Ziwasogo)
	if (_ethnicity == "african") then 
		{
		_civstring = [_civstrings select 4] call tpw_core_fnc_grabciv;
	
		// Screen out non  from BIS civs
		if (_civstring == "c_man") then
			{
			for "_i" from 0 to (count tpw_core_civs - 1) do	
				{	
				_civ = tpw_core_civs select _i;
				if (["asia",str _civ] call BIS_fnc_inString ) then
					{
					tpw_core_civs set [_i, -1];
					};
				};
			tpw_core_civs = tpw_core_civs - [-1];	
			};
		};
		
	// Asians (eg Prei Khmaoch Luong)
	if (_ethnicity == "asian") then 
		{
		_civstring = [_civstrings select 5] call tpw_core_fnc_grabciv;
	
		// Screen out non-Asians from BIS civs
		if (_civstring == "c_man") then
			{
			for "_i" from 0 to (count tpw_core_civs - 1) do	
				{	
				_civ = tpw_core_civs select _i;
				if !(["asia",str _civ] call BIS_fnc_inString) then
					{
					tpw_core_civs set [_i, -1];
					};
				};
			tpw_core_civs = tpw_core_civs - [-1];	
			};
		};	

	// Default - ethnic mix
	if (_ethnicity == "default") then 
		{
		_civstring = "c_man";
		_civstring = [_civstring] call tpw_core_fnc_grabciv;
	
		// Screen out unarmed combatants only
		if (_civstring == "c_man") then
			{
			for "_i" from 0 to (count tpw_core_civs - 1) do	
				{	
				_civ = tpw_core_civs select _i;
				if (["unarmed",str _civ] call BIS_fnc_inString) then
					{
					tpw_core_civs set [_i, -1];
					};
				};
			tpw_core_civs = tpw_core_civs - [-1];	
			};
		// Females, IDAP
		[] call tpw_core_fnc_grabfemciv;	
		//[] call	tpw_core_fnc_grabidapciv;
		};	

	// No pilot, diver, VR civs	
	for "_i" from 0 to (count tpw_core_civs - 1) do	
		{	
		_unit = tpw_core_civs select _i;
		if ((["pilot",str _unit] call BIS_fnc_inString)||(["diver",str _unit] call BIS_fnc_inString)||(["vr",str _unit] call BIS_fnc_inString)) then
			{
			tpw_core_civs set [_i, -1];
			};
		};	
		
	// User blacklist
	for "_i" from 0 to (count tpw_core_civs - 1) do	
		{	
		_unit = tpw_core_civs select _i;
			{
			if ([_x,str _unit] call BIS_fnc_inString) then
				{
				tpw_core_civs set [_i, -1];
				};
			} foreach tpw_core_blacklist;
		};
	tpw_core_civs = tpw_core_civs - [-1];
	
	// Prespawn to reduce stuttering later
		{
		_temp = _x createvehicle [0,0,1000]; 
		sleep 0.1;
		deletevehicle _temp;
		} count tpw_core_civs;
		
	tpw_core_ethnicity = _ethnicity;	
	};	

// HABITABLE HOUSES

// Core and DLC buildings
tpw_core_habitable = [ 
// OA classes - thanks Spliffz
"Land_House_L_1_EP1", 
"Land_House_L_3_EP1",
"Land_House_L_4_EP1",
"Land_House_L_6_EP1",
"Land_House_L_7_EP1",
"Land_House_L_8_EP1",
"Land_House_L_9_EP1",
"Land_House_K_1_EP1",
"Land_House_K_3_EP1", 
"Land_House_K_5_EP1", 
"Land_House_K_6_EP1", 
"Land_House_K_7_EP1", 
"Land_House_K_8_EP1", 
"Land_Terrace_K_1_EP1",
"Land_House_C_1_EP1",
"Land_House_C_1_v2_EP1", 
"Land_House_C_2_EP1", 
"Land_House_C_3_EP1",
"Land_House_C_4_EP1", 
"Land_House_C_5_EP1", 
"Land_House_C_5_V1_EP1", 
"Land_House_C_5_V2_EP1", 
"Land_House_C_5_V3_EP1", 
"Land_House_C_9_EP1", 
"Land_House_C_10_EP1", 
"Land_House_C_11_EP1", 
"Land_House_C_12_EP1", 
"Land_A_Villa_EP1",
"Land_A_Mosque_small_1_EP1",
"Land_A_Mosque_small_2_EP1",

//"Land_Ind_FuelStation_Feed_EP1",
"Land_Ind_FuelStation_Build_EP1",
"Land_Ind_FuelStation_Shed_EP1",
"Land_Ind_Garage01_EP1",
"Land_A_Mosque_big_minaret_1_EP1",
"Land_A_Mosque_big_hq_EP1",

// A2 classes - thanks Reserve
"Land_HouseV_1I1",  
"Land_HouseV_1I2",
"Land_HouseV_1I3",
"Land_HouseV_1I4",
"Land_HouseV_1L1",
"Land_HouseV_1L2",
"Land_HouseV_1T",
"Land_HouseV_2I",
"Land_HouseV_2L",
"Land_HouseV_2T1",
"Land_HouseV_2T2",
"Land_HouseV_3I1",
"Land_HouseV_3I2",
"Land_HouseV_3I3",
"Land_HouseV_3I4",
"Land_HouseV2_01A",
"Land_HouseV2_01B",
"Land_HouseV2_02",
"Land_HouseV2_03",
"Land_HouseV2_03B",
"Land_HouseV2_04",
"Land_HouseV2_05",
"Land_HouseBlock_A1",
"Land_HouseBlock_A2",
"Land_HouseBlock_A3",
"Land_HouseBlock_B1",
"Land_HouseBlock_B2",
"Land_HouseBlock_B3",
"Land_HouseBlock_C2",
"Land_HouseBlock_C3",
"Land_HouseBlock_C4",
"Land_HouseBlock_C5",
"Land_Church_02",
"Land_Church_02A",
"Land_Church_03",
"Land_A_FuelStation_Build",
"Land_A_FuelStation_Shed",

// Fallujah
"Land_Dum_istan3_hromada2",
"Land_Sara_stodola2",
"Land_dum_istan2",
"Land_dum_istan2b",
"Land_dum_istan2_01",
"Land_dum_istan2_02",
"Land_dum_istan2_03",
"Land_dum_istan2_03a",
"Land_dum_istan2_04a",
"Land_dum_istan3",
"Land_dum_istan3_hromada",
"Land_dum_istan4",
"Land_dum_istan4_big",
"Land_dum_istan4_big_inverse",
"Land_dum_istan4_detaily1",
"Land_dum_istan4_inverse",
"Land_dum_mesto3_istan",
"Land_hotel",
"Land_stanek_1",
"Land_stanek_1b",
"Land_stanek_1c",
"Land_stanek_2",
"Land_stanek_2b",
"Land_stanek_2c",
"Land_stanek_3",
"Land_stanek_3b",
"Land_stanek_3c",



"Land_sara_domek_zluty",
"Land_sara_domek_sedy",
"Land_dum_mesto2",
"Land_dum_mesto_in",

// Montella
"Land_Panelak3",
"Land_Panelak",
"Land_Panelak2",
"Land_Hospoda_mesto",
"Land_Cihlovej_dum",
"Land_Cihlovej_Dum_mini",
"Land_Dum02",
"Land_Stanice",
"Land_Sara_domek_ruina",
"Land_Dum_olezlina",
"Land_Kostel3",
"Land_Sara_domek04",
"Land_Brana02nodoor",
"Land_Sara_domek05",
"Land_Dum_m2",
"Land_Deutshe_mini",
"Land_Dum_mesto3",
"Land_OrlHot",
"Land_Sara_domek03",
"Land_Cihlovej_Dum_in",
"Land_Sara_domek01",
"Land_Sara_domek_rosa"];

tpw_core_fnc_houses =
	{
	private ["_housearray","_radius","_return"];
	_housearray = [];
	_radius = _this select 0;
	_return = nearestObjects [position vehicle player,tpw_core_habitable,_radius]; 
	_return = _return select {!(["_u_", str (typeof _x)] call BIS_fnc_inString)};
	_return
	};
	
tpw_core_fnc_screenhouses =
	{
	private ["_radius","_return"];
	_radius = _this select 0;
	_return = tpw_core_habhouses select {_x distance player < _radius};
	_return	
	};
	
// SUN ANGLE - ORIGINAL CODE BY CARLGUSTAFFA
tpw_core_fnc_sunangle =
	{
	private ["_lat","_day","_hour"];
	while {true} do 
		{
		_lat = -1 * getNumber(configFile >> "CfgWorlds" >> worldName >> "latitude");
		_day = 360 * (dateToNumber date);
		_hour = (daytime / 24) * 360;
		tpw_core_sunangle = ((12 * cos(_day) - 78) * cos(_lat) * cos(_hour)) - (24 * sin(_lat) * cos(_day));  
		sleep 33.33; 
		};
	};	
	
// DETERMINE UNIT'S WEAPON TYPE 
tpw_core_fnc_weptype =
	{
	private["_unit","_weptype","_cw","_hw","_pw","_sw"];
	_unit = _this select 0;	
	
	// Weapon type
	_cw = currentweapon _unit;
	_hw = handgunweapon _unit;
	_pw = primaryweapon _unit;
	_sw = secondaryweapon _unit;
	 switch _cw do
		{
		case "": 
			{
			_weptype = 0;
			};
		case _hw: 
			{
			_weptype = 1;
			};
		case _pw: 
			{
			_weptype = 2;
			};
		case _sw: 
			{
			_weptype = 3;
			};
		default
			{
			_weptype = 0;
			};	
		};
	_unit setvariable ["tpw_core_weptype",_weptype];
	};
/*	
// DISABLE UNIT ON GROUND
tpw_core_fnc_disable = 
	{
	private ["_unit"];
	_unit = _this select 0;
	if !(alive _unit) exitwith {};
	_unit setunitpos "DOWN";
	_unit switchmove "acts_InjuredLyingRifle02_180";
	{_unit disableai _x} count ["anim","move","fsm","target","autotarget"];
	_unit setvariable ["tpw_core_disabled",1];
	
	};

// RE-ENABLE UNIT
tpw_core_fnc_enable = 
	{
	private ["_unit"];
	_unit = _this select 0;
	sleep random 5;
	{_unit enableai _x} count ["anim","move","fsm","target","autotarget"];
	_unit setunitpos "UP";
	[_unit] call tpw_core_fnc_weptype;
	_unit switchmove "AinjPpneMstpSnonWnonDnon_rolltofront";
	_unit playmove (['AmovPpneMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon',"AmovPercMstpSrasWpstDnon","AmovPercMstpSrasWrflDnon","AmovPercMstpSrasWlnrDnon"] select (_unit getvariable "tpw_core_weptype"));
	_unit setvariable ["tpw_core_disabled",0];
	_unit setcaptive false;
	};	
*/
// DISABLE UNIT ON GROUND
tpw_core_fnc_disable = 
	{
	private ["_unit","_anims"];
	_unit = _this select 0;	
	if !(alive _unit) exitwith {};

	_anims = ["Acts_InjuredAngryRifle01","Acts_InjuredCoughRifle02","Acts_InjuredLookingRifle01","Acts_InjuredLookingRifle02","Acts_InjuredLookingRifle03","Acts_InjuredLookingRifle04","Acts_InjuredLookingRifle05","Acts_LyingWounded_loop3","Acts_CivilInjuredArms_1","Acts_CivilInjuredChest_1","Acts_CivilInjuredGeneral_1","Acts_CivilInjuredHead_1","Acts_CivilInjuredLegs_1"];	
	
	_unit addeventhandler ["Killed",{_unit = _this select 0;if (tolower (animationstate _unit) in ["acts_injuredangryrifle01","acts_injuredcoughrifle02","acts_injuredlookingrifle01","acts_injuredlookingrifle02","acts_injuredlookingrifle03","acts_injuredlookingrifle04","acts_injuredlookingrifle05","acts_lyingwounded_loop3","acts_civilinjuredarms_1","acts_civilinjuredchest_1","acts_civilinjuredgeneral_1","acts_civilinjuredhead_1","acts_civilinjuredlegs_1"]) then {_unit setdir (getdir _unit + 180);_unit switchmove "AinjPpneMstpSnonWnonDnon"}}]; 

	_unit setunitpos "DOWN";
	_unit setdir (getdir _unit + 180);
	_unit switchmove (_anims select floor random count _anims);
	{_unit disableai _x} count ["anim","move","fsm","target","autotarget"];
	_unit setvariable ["tpw_core_disabled",1];
	};

// RE-ENABLE UNIT
tpw_core_fnc_enable = 
	{
	private ["_unit"];
	_unit = _this select 0;
	if !(alive _unit) exitwith {};
	sleep random 5;
	{_unit enableai _x} count ["anim","move","fsm","target","autotarget"];
	_unit setunitpos "UP";
	[_unit] call tpw_core_fnc_weptype;
	_unit setdir (getdir _unit + 180);
	_unit switchmove "AinjPpneMstpSnonWnonDnon_rolltofront";
	_unit playmove (['AmovPpneMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon',"AmovPercMstpSrasWpstDnon","AmovPercMstpSrasWrflDnon","AmovPercMstpSrasWlnrDnon"] select (_unit getvariable "tpw_core_weptype"));
	_unit setvariable ["tpw_core_disabled",0];
	_unit setcaptive false;
	};		
	

// SORT ARRAY OF OBJECTS BASED ON DISTANCE TO PLAYER
tpw_core_fnc_arraysort =
	{
	private _origarray = _this select 0;
	private _distarray = _origarray apply {[_x distance player, _x]}; 
	_distarray sort true;
	private _sortedarray = [];
	{_sortedarray pushback (_x select 1)} foreach _distarray;	
	_sortedarray
	};
	
tpw_core_fnc_arrayrevsort =
	{
	private _origarray = _this select 0;
	private _distarray = _origarray apply {[_x distance player, _x]}; 
	_distarray sort false;
	private _sortedarray = [];
	{_sortedarray pushback (_x select 1)} foreach _distarray;	
	_sortedarray
	};	
	
// SCAN WHETHER THERE IS NEARBY BATTLE (GUNFIRE/EXPLOSIONS/GRENADES)	
// tpw_core_battletime is advanced by 1-2 minutes every time these events occur
tpw_core_fnc_battle = 
	{
	while {true} do
		{
		if (diag_ticktime > tpw_core_battletime) then	
			{
			tpw_core_battle = false;
			} else
			{
			tpw_core_battle = true;
			};
		sleep 2;	
		};
	};

// NEARBY GUNFIRE
tpw_core_battle = false;
tpw_core_battletime = 0;
player addeventhandler ["firednear",{tpw_core_battletime = diag_ticktime + 60 + random 120}];	
0 = [] spawn tpw_core_fnc_battle;	
		
// CALL OR SPAWN APPROPRIATE FUNCTIONS
tpw_core_housescanflag = 0;
//0 = [] spawn tpw_core_fnc_sunangle;	
[tpw_core_mapstrings] spawn tpw_core_fnc_civs;

// NEARBY HABITABLE HOUSES
tpw_core_allhouses = [50000] call tpw_core_fnc_houses;
tpw_core_habitable = [];
_habitable = [];
	{
	_type = typeof _x;
	if !(_type in tpw_core_habitable) then
		{
		_habitable pushback _type;
		};
	} foreach tpw_core_allhouses;
tpw_core_habitable = _habitable;
_habitable = [];	

tpw_core_habhouses = [];
sleep 10;
_lastpos = [0,0,0];
while {true} do
	{
	if ((speed player < 20) && {player distance _lastpos > 250}) then
		{
		//tpw_core_habhouses = [500] call tpw_core_fnc_houses;
		tpw_core_habhouses = tpw_core_allhouses select {_x distance player < 500};
		_lastpos = position player; 
		};
	sleep 30 + (count tpw_core_habhouses)/2; 
	};
	
	
// DUMMY LOOP SO SCRIPT DOESN'T TERMINATE
while {true} do
	{
	sleep 10;
	};	