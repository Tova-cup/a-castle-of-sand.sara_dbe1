class Params
{
	class TOV_dummyFmRadio
	{
		title = "Use dummy radio lengths";
		values[] = {0, 1};
		texts[] = {"No", "Yes"};
		default = 0;
	};
};	