//Bus system
//How bus stop markers were retrieved
/*busStops =
{
	params[["_layerName",""]];
	_myLayer = -1 add3DENLayer _layerName;
	_num=0;
	{
		if ((str _x find "zastavka_jih" > -1) || (str _x find "zastavka_stojan" > -1)) then
		{
			_id = "busStop" +"_" + str _num;
			_pos = getPos _x;
			_marker = create3DENEntity ["Marker", "hd_dot", _pos];
			_marker set3DENLayer _myLayer;
			_marker set3DENAttribute ["markername", _id];
			_num=_num+1;
		}	
	} forEach (nearestTerrainObjects [[worldSize/2,worldSize/2,0],[], worldSize*2]);
};

["bus-Stops"] call busStops;
*/

TOV_bus_spawned=[];

TOV_call_bus={
	params ["_caller","_busStop"];
	
	_destination=getPos _busStop;
	_initialStop=getPos _caller;
	
	_shouldSkip={
		params["_driver"];
		if (unitReady _driver || !alive _driver || !canMove vehicle _driver || vehicle _driver==_driver ) then {true;}
		else {false};
	};
	
	_findNearestRoadPos={
		params ["_destination",["_radius",100],["_reversePos",false],["_reference",nil]];
		if (isNil "_reference") then {_reference=_destination};
		_roads=(_destination nearRoads _radius);
		_roads = _roads apply { [_x distance _reference, _x] };
		_roads sort true;
		if (_reversePos) then {reverse _roads};
		if (count _roads==0) exitWith {[]};
		_destination=_roads#0#1;
		getposATL _destination;
	};
	
	
	_drirectionOfTravel=player getDir _destination;
	_spawnPos=[];
	while {count _spawnPos==0} do {
		_spawnPos=player getPos [500, (_drirectionOfTravel+180)%360];
		_spawnPos=[_spawnPos,500,true,player] call _findNearestRoadPos;
		_drirectionOfTravel=(_drirectionOfTravel+20)%360;
	};
	
	//_spawnPos=getPos player;
	//_spawnPos=[_spawnPos,500,true] call _findNearestRoadPos;
	_driver = (createGroup [civilian,true]) createUnit ["C_man_1",_spawnPos,[],0,"NONE"];  
	//_driver moveInDriver ("CUP_C_Ikarus_TKC" createVehicle position _driver);
	_driver moveInDriver ("C_Van_01_transport_F" createVehicle position _driver);
	//vehicle _driver forceFollowRoad true;
	
	_driver setVariable ["assignedBusStop",_initialStop,true];
	TOV_bus_spawned pushback _driver;
	publicVariable "TOV_bus_spawned";
	
	hint str _driver;
	_driver disableAI "ALL";
	{_driver enableAI _x} forEach ["MOVE","PATH","LIGHTS","ANIM","TEAMSWITCH"];

	vehicle _driver setDir (vehicle _driver getDir _destination);
	//_driver setBehaviour "CARELESS";
	_driver setSpeedMode "LIMITED";
	
	_passengers=random ((vehicle _driver emptyPositions "cargo")-4);
	for "_i" from 0 to _passengers do {
		_civ=createAgent ['C_man_polo_3_F', getpos _driver, [], 0, 'NONE'];
		_civ call TOV_civ_setupCiv;
		_civ moveInCargo vehicle _driver;
		sleep 0.05;
	};
	
	_wp=group  _driver addWaypoint [[_initialStop] call _findNearestRoadPos, 0];
	_wp setWaypointTimeout [10, 10, 10];
	_destination=[_destination] call _findNearestRoadPos;
	_wp=group  _driver addWaypoint [_destination, 0];
	_wp setWaypointTimeout [10, 10, 10];
	_destination=_destination getPos [300, getDir _driver];
	_roads=(_destination nearRoads 300) select { _x distance _destination>200};
	_destination = selectRandom _roads;
	if (count _roads==0) then {
		_destination=getpos (selectRandom (( _destination nearRoads 500) select { _x distance _destination>200}));
	} else {_destination = getpos _destination};
	_wp=group  _driver addWaypoint [_destination, 0];
	
	sleep 10;
	waitUntil {[_driver] call _shouldSkip && ({_driver distance _x<150} count allPlayers==0)};
	{deleteVehicle _x} forEach crew vehicle _driver + [vehicle _driver];
	TOV_bus_spawned=TOV_bus_spawned - [_driver];
	publicVariable "TOV_bus_spawned";
	hint "deleted";
	if (player distance _initialStop<10) then {hint "Next bus is cancelled because of strikes."};
};

_bus_stops=[];
TOV_bus_stops_array=[];
{
	if ((str _x find "zastavka_jih" > -1) || (str _x find "zastavka_sever" > -1)) then
	{
		_bus_stops pushback _x;
	}	
} forEach (nearestTerrainObjects [[worldSize/2,worldSize/2,0],[], worldSize*2]);
//hint str _bus_stops;

{
	TOV_bus_stops_array pushback (
	[
		getpos _x,
		{(_this # 9) spawn TOV_call_bus },
		"Bus Stop",
		"",
		"",
		"",
		1.5,
		[ player, _x]
	]
	);
	
} forEach _bus_stops;
	



/*
{
	_x addAction ["<t color='#FF0000'>Wait for the bus</t>",
	{
		[
			findDisplay 46,
			getPos player,
			TOV_bus_stops_array,
			[],
			[],				//markers
			[],				//images
			0,				//weather
			false,			//night
			100,				//scale
			true,			//simulation
			"Select bus destination",	//title
			false
		] call BIS_fnc_StrategicMapOpen;
	}
	];
} forEach _bus_stops;*/

{
	[
		_x,											// Object the action is attached to
		"Wait for the bus",										// Title of the action
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",	// Idle icon shown on screen
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",	// Progress icon shown on screen
		"_this distance _target < 3",						// Condition for the action to be shown
		"_caller distance _target < 3",						// Condition for the action to progress
		{},													// Code executed when action starts
		{},													// Code executed on every progress tick
		{  
			if ( ({_target distance (_x getVariable "assignedBusStop")<10 } count TOV_bus_spawned) ==0) then {
				[
					findDisplay 46,
					getPos player,
					TOV_bus_stops_array,
					[],
					[],				//markers
					[],				//images
					0,				//weather
					false,			//night
					100,				//scale
					true,			//simulation
					"Select bus destination",	//title
					false
				] call BIS_fnc_StrategicMapOpen;
				
			} else {
				_currentBus=TOV_bus_spawned select {_target distance (_x getVariable "assignedBusStop")<10};
				_currentBus=_currentBus#0;
				_timeToWait=round (((_target distance _currentBus)/(40/3.6))/60);
				hint format ["Next bus : %1 min",_timeToWait];
			};
			
		},				// Code executed on completion
		{},													// Code executed on interrupted
		[],													// Arguments passed to the scripts as _this select 3
		3,													// Action duration [s]
		0,													// Priority
		false,												// Remove on completion
		false												// Show in unconscious state 
	] call BIS_fnc_holdActionAdd;
} forEach _bus_stops;

