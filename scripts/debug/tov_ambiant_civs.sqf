if(!isServer)exitWith{};

params[["_maxCivs",50],["_blacklistMarkers",[]]];

TOV_civ_max=_maxCivs;
TOV_civ_array=[];
TOV_civ_civPerHouse=200;
TOV_civ_towns=[];
execVM "scripts\debug\tov_civs_clothes.sqf";


//is combat happening ?
/*{
	_x addeventhandler ["firednear",{ _unit=_this#0; _unit setVariable ["TOV_civ_endOfDanger",(call TOV_civ_time) + 60 + random 120],true}];
} forEach allPlayers;
*/

{
	TOV_civ_time={
		if (isMultiplayer) then {
			serverTime;
		} else {
			time;
		};
	};

	player addeventhandler ["firednear",{ _unit=_this#0; _unit setVariable ["TOV_civ_endOfDanger",(call TOV_civ_time) + 60 + random 120,true]}];
} remoteExec ["bis_fnc_call", 0,true]; 



//Code to be run on each civ spawn
TOV_civ_setupCiv={
	params ["_civ",["_myTrigger",nil]];
	_civ setcaptive true;
	_civ setBehaviour 'CARELESS'; 
	_civ disableAI 'FSM';
	removeallweapons _civ;
	removegoggles _civ;	
	removevest _civ;
	removebackpack _civ;
	removeHeadgear _civ;
	_civ forceAddUniform selectRandom tov_civs_uniforms;
	if (random 1 < 0.3) then {
		_civ addHeadgear selectRandom tov_civs_hats;
	};
	_civ setAnimSpeedCoef 0.8 + random 0.2;
	if !(isNil "_myTrigger") then {
		[_civ,_myTrigger] execFSM 'scripts\debug\civAI.fsm';
	};
};




//Setting up the civilian zones
TOV_civ_triggers=[];
_townsConfig=(configFile>>"CfgWorlds">>worldName>>"Names");

for "_i" from 0 to ((count _townsConfig) - 1) do
{	
	//_towns select _x;
	_currentTown=_townsConfig select _i;
	
	_townName=getText(_currentTown>>"name");
	_townPos=getArray(_currentTown>>"position");
	_townX=getNumber(_currentTown>>"radiusA");
	_townY=getNumber(_currentTown>>"radiusB");

	_tArea=_townX max _townY;
	_townDir=getNumber(_currentTown>>"angle");
	
	
	if ((count(_townPos nearObjects["House",_tArea])>5) && _tArea>0 && _townName!="") then {
		_tArea=_tArea*1.25;
		
		///////////////DEBUG////////////////////
		_m=createMarker[format["m--%1",_townPos],_townPos];
		_m setMarkerSize[_tArea,_tArea];
		_m setMarkerShape"ELLIPSE";
		_m setMarkerBrush "SolidBorder";
		_m setMarkerAlpha 0.4;
		_m setMarkerColor"ColorCiv";
		_m setMarkerDir _townDir;
		_m setMarkerText _townName;
		//hint _townName;
		TOV_civ_towns pushBack (_townName);
		///////////////END OF DEBUG////////////////////
		
		_t=createTrigger["EmptyDetector",_townPos,false];
		_t setTriggerArea[_tArea,_tArea,_townDir,false];
		_t setTriggerTimeout[2,2,2,true];
		_t setTriggerActivation["ANYPLAYER","PRESENT",TRUE];
		
		sleep 0.02;
		
		
		_t setVariable ["_dog","woof"];
		_t setVariable ['_tCivArray',[]];
		TOV_civ_triggers pushBack _t;
		
		_t setTriggerStatements[
		//TRIGGER CONDITION
		"count thisList>0",

		//TRIGGER ACTIVATION
		"
		thisTrigger spawn {
			_myTrigger=_this;
			_tpos=getpos _myTrigger;
			_tArea=triggerArea _myTrigger #0;

			_useableBuildings=nearestObjects [_tPos, ['house','building'], _tArea+40];
			_myTrigger setVariable ['_useableBuildings',_useableBuildings];
			_civsToSpawn=count 	_useableBuildings*TOV_civ_civPerHouse min TOV_civ_max;

			{ 
				_x setVariable ['TOV_civ_endOfDanger',0,true];
			} forEach (allPlayers select {_x distance _tpos <(_tArea+150) } );

			while { triggerActivated _myTrigger } do {
				_tCivArray=_myTrigger getVariable '_tCivArray';
				_myTrigger setVariable ['_tCivArray',_tCivArray];
				
				if ((count (_tCivArray select {alive _x})<_civsToSpawn) && !(_myTrigger getVariable ['_isDanger',false])) then {
					_closePositions=[player];
					{	
						_currentPlayer=_x;
						_tmp=_useableBuildings select {_x distance _currentPlayer<75};
						_closePositions=_closePositions+_tmp;
					} forEach allPlayers;
					_spawnPos=getPos selectRandom _closePositions;
					_spawnPos=[_spawnPos, 0, 30, 0, 0] call BIS_fnc_findSafePos;
					_spawnPos set [2,getTerrainHeightASL _spawnPos];
					_spawnPos= ASLToATL _spawnPos;
					_spawnCheck=[_spawnPos#0,_spawnPos#1,_spawnPos#2 +1.6];

					_seenBy = allPlayers select {_x distance _spawnPos < 15 || {(_x distance _spawnPos < 300 && {([vehicle _x,'VIEW'] checkVisibility [eyePos _x,ATLToASL _spawnCheck]) > 0.1})}};
					 if (count _seenBy==0) then {
						_civ=createAgent ['C_man_polo_3_F', _spawnpos, [], 0, 'NONE'];
						
						[_civ,_myTrigger] call TOV_civ_setupCiv;
						
						_tCivArray pushBack (_civ);				
						sleep 0.05;
					 };
					
				} else {
					{
						_currentCiv=_x;
						_seenBy = allPlayers select {_x distance _currentCiv < 15 || {(_x distance _currentCiv < 300 && {([vehicle _x,'VIEW'] checkVisibility [eyePos _x, eyePos _currentCiv]) > 0.1})}};
						if (({_currentCiv distance _x<100} count allPlayers==0 ) && (count _seenBy ==0)) then {
							_tCivArray=_tCivArray-[_currentCiv];
							deletevehicle _currentCiv;
							sleep 0.05;
						};
					} forEach _tCivArray;
				};
				
				_myTrigger setVariable ['_tCivArray',_tCivArray];
				sleep 0.1;
				
				_dangerTimes=[0];
				{ 
					_dangerTimes pushBack (_x getVariable ['TOV_civ_endOfDanger',0]);
				} forEach (allPlayers select {_x distance _tpos <_tArea } );
				_dangerTimes= selectMax _dangerTimes;
				
				if (_dangerTimes>(call TOV_civ_time)) then {
					_myTrigger setVariable ['_isDanger',true];
				} else {
					_myTrigger setVariable ['_isDanger',false];
				};
			};
		}		
		",

		//TRIGGER DEACTIVATION
		"
		thisTrigger spawn {
			_myTrigger=_this;
			_myTrigger setVariable['_isOn',false];
			
			_tCivArray=_myTrigger getVariable '_tCivArray';
			while { !(triggerActivated _myTrigger) && (count (_myTrigger getVariable '_tCivArray')>0) } do {
				
				{
					_currentCiv=_x;
					_seenBy = allPlayers select {_x distance _currentCiv < 15 || {(_x distance _currentCiv < 300 && {([vehicle _x,'VIEW'] checkVisibility [eyePos _x, eyePos _currentCiv]) > 0.1})}};
					if (({_currentCiv distance _x<50} count allPlayers==0 ) && (count _seenBy ==0)) then {
						_tCivArray=_tCivArray-[_currentCiv];
						deletevehicle _currentCiv;
						sleep 0.1;
					};
				} forEach _tCivArray;
				
				sleep 0.5,
				_myTrigger setVariable ['_tCivArray',_tCivArray];
			};
		}		
		"];
	
	};

};

publicVariable "TOV_civ_triggers";

//Check if we already have an array of civs
//Get list of buildings
//get list of house
//calculate how many civs can be spawned
//start the spawning (don't forget to add a condition in case trigger gets deactivated)

//params ['_tPos','_tArea','_civPerHouse','_tCivArray','_randomClothing'];

/*

					_spawnPos=[_spawnPos, 0, 30, 0, 0] call BIS_fnc_findSafePos;
					_spawnPos pushBack 0;

*/