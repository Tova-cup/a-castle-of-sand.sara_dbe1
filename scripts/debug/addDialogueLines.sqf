//Test benchmark for Talk script :
// there must be 4 units names a_1,a_2 ... on the map for this to work

[a_1,{
	params ["_target", "_caller", "_actionId", "_arguments"];
	/*
	[_target,"I'm gangsta !<br/> You have to press 'skip' to get to my next line, dog.","CENTERDIRECT",true] call TOV_Talk;
	[_target,"And like my hoes, you can move those hips while talking to me.","CENTERDIRECT",true] call TOV_Talk;
	[_target,"But I ain't no fool !<br/> If you go too far I'll stop talking cuz' I ain't speaking to no homie who can't hear me !","CENTERDIRECT",true] call TOV_Talk;
	*/
	
	_name=name _caller;
	_line= format["Daaaaamn ! Look at my homie %1 !",_name];
	[_target,_line,"CENTERDIRECT",true] call TOV_Talk;
	[_target,"As you can see, I'm still a real OG !<br/> You gotta press skip to read my next lines","CENTERDIRECT",true] call TOV_Talk;
	[_target,"By the way, cool to see you back in the hood dog !","CENTERDIRECT",true] call TOV_Talk;
	[_target,"But hey !","CENTERDIRECT",true] call TOV_Talk;
	[_target,"Never forget who's the boss around here.","CENTERDIRECT",true] call TOV_Talk;
},false] call TOV_addTalkAction;

[a_2,{
	params ["_target", "_caller", "_actionId", "_arguments"];
	/*
	[_target,"I'm a bit too... tactile !<br/> You can't move while talking to me cuz' I'm grabbing you by the arm.","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"But I'm still a weak ass dude, so you can press 'skip' to move quicker through my lines","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"Soo before you leave...<br/> Do you have a cigarette to spare my friend ?","CENTERDIRECT",true,false] call TOV_Talk;
	*/
	
	[_target,"*Obviously intoxicated, he grabs you by the arm.*","DESCRIPTION",true,false] call TOV_Talk;
	[_target,"Hey yooo mister 'I don't have any smoke'.","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"Y'know, we're square even tho you didn't share yesterday,","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"Cuz' later in the evening, my boy Gekas shared the fattest joint I've ever smoked !","CENTERDIRECT",true,false] call TOV_Talk;
	
},false] call TOV_addTalkAction;

[a_3,{
	params ["_target", "_caller", "_actionId", "_arguments"];
	
	/*
	[_target,"I'm a suave playboy, my charisma is unlimited !<br/> You can neither move nor skip my lines while I'm speaking.","CENTERDIRECT",false,false] call TOV_Talk;
	[_target,"You are so hypnotised by my manly voice, you have to listen to aaaall I'm saying...","CENTERDIRECT",false,false] call TOV_Talk;
	[_target,"While saying it at my...","CENTERDIRECT",false,false] call TOV_Talk;
	[_target,"...own...","CENTERDIRECT",false,false] call TOV_Talk;
	[_target,"...pace...","CENTERDIRECT",false,false] call TOV_Talk;
	[_target,"Alright, go now, I have women to seduce.","CENTERDIRECT",false,true] call TOV_Talk;
	*/
	
	[_target,"Even though I'm still the most handsome living being to have ever walked the sands of Sahrani,","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"I have to admit I couldn't get any woman in my bed yesterday...","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"It's not that I couldn't SEDUCE them,","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"It's that I couldn't FIND any !","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"What kind of island did I get trapped on ?!","CENTERDIRECT",true,false] call TOV_Talk;
	[_target,"Oh god !<br/> Is this the punishment for my sins ?!","CENTERDIRECT",true,false] call TOV_Talk;
	
},false] call TOV_addTalkAction;

[a_4,{
	params ["_target", "_caller", "_actionId", "_arguments"];
	/*
	[_target,"This is Misfit 2-1 !<br/> I'm speaking to you through my radio.","CENTERSIDE"] call TOV_Talk;
	[_target,"Only YOU can hear my lines and the radio sound effect.<br/> BTW you can't skip my lines.","CENTERSIDE"] call TOV_Talk;
	[_target,"Misfit 2-1 Out !","CENTERSIDE"] call TOV_Talk;*/
	
	[_target,"This is Misfit 2-1 !<br/> I'm speaking to you through my radio.","CENTERSIDE"] call TOV_Talk;
	[_target,"I think command forgot to send someone to relieve me !","CENTERSIDE"] call TOV_Talk;
	[_target,"Can you please let them know ?<br/> I'd rather not leave and get caught AWOL.","CENTERSIDE"] call TOV_Talk;
	[_target,"Misfit 2-1 Out !","CENTERSIDE"] call TOV_Talk;
	
},false] call TOV_addTalkAction;
