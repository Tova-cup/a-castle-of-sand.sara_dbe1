//params ["_myUnits","_destination",["_finalBehaviour","AWARE"],["_finalStance","AUTO"]];
TOV_moveAndStay = compile (preprocessFileLineNumbers "scripts\common\moveAndStay.sqf");

//params [["_layer",""],["_filter",""],["_deleteAfterGrab",false]];
TOV_grabPosFromLayer = compile (preprocessFileLineNumbers "scripts\common\grabPositionsFromLayer.sqf");

//params ["_speaker", "_sentence", "_speakingMode", ["_skippable",false], ["_canMove",true], ["_autoSkip",true], ["_readingTime",nil], ["_size",1.2]];
TOV_Who_Speaking=objNull;
TOV_talk = compile (preprocessFileLineNumbers "scripts\common\dialogSystem.sqf");

//params ["_target","_talkCode",["_removeOnCompletion",true]];
//talk code params given as _this are params ["_target", "_caller", "_actionId", "_arguments"];
TOV_addTalkAction= compile (preprocessFileLineNumbers "scripts\common\addTalkAction.sqf");

//edited BIS dynamicText to fit my needs, must be spawned now
TOV_fnc_dynamicText= compile (preprocessFileLineNumbers "scripts\common\BIS_fnc_dynamicText_edited.sqf");

//params["_target","_text","_value","_fsmHandle"];
TOV_addTalkChoiceFSM= compile (preprocessFileLineNumbers "scripts\common\addTalkChoiceFSM.sqf");

//params ["_classname", "_position",["_direction",0],["_noMove",false],["_anim",""]];
TOV_agentSpawner=compile (preprocessFileLineNumbers "scripts\common\agentSpawner.sqf");
