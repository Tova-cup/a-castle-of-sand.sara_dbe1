//Intro of the mission
//[] call BIS_fnc_cameraOld;
//execVM "missions\0-Intro\0-intro.sqf"

cutText ["", "BLACK FADED", 30, true, true];
0 fadeSound 0;
_oldDate=date;
_initialPos= getpos player;
player setPos [13201.5,8898.04,0.00143814];

_trash=[];

if (isServer) then {
	_trash pushback	(["C_man_1",[13152.9,8829.6,0.110001],300.389,true,"Acts_CivilTalking_1"] call TOV_agentSpawner);
	_trash pushback	(["C_Man_casual_1_F",[13151.8,8829.55,0.109999],60.3593,true,"Acts_CivilTalking_2"] call TOV_agentSpawner);
	_trash pushback (["CUP_C_Ikarus_Chernarus",[12944.1,8851.3,0],80.9521] call TOV_agentSpawner);
	_trash pushback (["C_Man_Fisherman_01_F",[13018.2,9079.43,5.84664],25.044,true,"HubSittingChairB_idle2"] call TOV_agentSpawner);
	_trash pushback (["Land_CampingChair_V1_F",[13018.2,9079.4,5.83751],177.83] call TOV_agentSpawner);
	_trash pushback (["C_Man_ConstructionWorker_01_Red_F",[13296.5,9092.11,0],82.573,true,"Acts_CivilListening_1"] call TOV_agentSpawner);
	_trash pushback (["C_Man_ConstructionWorker_01_Red_F",[13296.7,9093.43,0],132.911,true,"Acts_CivilListening_2"] call TOV_agentSpawner);
	_trash pushback (["C_Offroad_01_repair_F",[13294.5,9097.28,0],326.882] call TOV_agentSpawner);
	_trash pushback (["Land_ClothesLine_01_full_F",[13191.3,8924.08,12.4837],51.3313] call TOV_agentSpawner);
	_trash pushback (["C_Man_casual_1_F_euro",[13193.6,8922.35,13.2576],219.17,true,"Acts_Accessing_Computer_Loop"] call TOV_agentSpawner);
	_trash pushback (["CUP_I_M60A3_RACS",[13245,9171.63,0],179.15] call TOV_agentSpawner);

};


_timeLapse= { 
 params [["_howMuch",0.033],["_sleepLenght",0.01]]; 
 while {true} do { 
  skiptime _howMuch;  
  sleep _sleepLenght; 
 }; 
};

setDate [2020,7,10,5,09];
playMusic "HouseOfCards";
_camera = "camera" camCreate [0,0,0];
_camera cameraEffect ["Internal","back"];
showCinemaBorder false;




_camera camPrepareTarget [15720.7,9942.18,0];
_camera camPreparePos [12736.8,8700.65,38.0432];
_camera camPrepareFOV 0.31;
//setAperture 0;
//_camera setDir 67.4092;
//[_camera, 4.1197, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};




sleep 1;
["<t font='PuristaMedium' size = '2'>An Arma 3 mission</t>",-1,-1,2,0.5,0] spawn tov_fnc_dynamicText; 
sleep 4;
cutText ["", "BLACK IN", 0.5, true, true];

//_tLapse=[] spawn _timeLapse;

_camera camPrepareTarget [15720.7,9942.18,0];
_camera camPreparePos [12736.8,8700.65,38.0432];
_camera camPrepareFOV 0.29;
//setAperture 0;
//_camera setDir 67.4092;
//[_camera, 4.1197, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
["<t font='PuristaLight' size = '1.5'></t>",-0.5,0.15,10,0.5,0] spawn tov_fnc_dynamicText; 
waitUntil {camCommitted _camera};
//terminate _tLapse;




_camera camPrepareTarget [13096,8777.43,0];
_camera camPreparePos [12787.7,8828.47,29.723];
_camera camPrepareFOV 0.34;
//setAperture 0;
//_camera setDir 99.3984;
//[_camera, -6.93896, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>An RPG experience</t>",0.5,0.5,4,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13095.8,8781.11,0];
_camera camPreparePos [12788.2,8832.02,29.3437];
_camera camPrepareFOV 0.34;
//setAperture 0;
//_camera setDir 99.3984;
//[_camera, -6.93896, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [12218.3,8832.63,0];
_camera camPreparePos [13322.3,8843.84,8.09495];
_camera camPrepareFOV 0.4;
//setAperture 0;
//_camera setDir 269.419;
//[_camera, 2.33327, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>Designed for single-player</t>",0.4,0.8,4,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [12216.3,8834.1,0];
_camera camPreparePos [13322.3,8845.33,8.09495];
_camera camPrepareFOV 0.4;
//setAperture 0;
//_camera setDir 269.419;
//[_camera, 2.33327, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 2.5;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13418,8901.71,0]; 
_camera camPreparePos [13512.6,8985.96,15.7364]; 
_camera camPrepareFOV 0.4; 
//setAperture 0; 
//_camera setDir 228.298; 
//[_camera, -7.39768, 0] call BIS_fnc_setPitchBank; 
_camera camCommitPrepared 0; 
//waitUntil {camCommitted _camera}; 
["<t font='PuristaLight' size = '1.5'>But CO-OP compatible</t>",0,0.8,4,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13415.2,8903.03,0]; 
_camera camPreparePos [13510.7,8988.11,15.7364]; 
_camera camPrepareFOV 0.4; 
//setAperture 0; 
//_camera setDir 228.298; 
//[_camera, -7.39768, 0] call BIS_fnc_setPitchBank; 
_camera camCommitPrepared 3; 
waitUntil {camCommitted _camera}; 



_camera camPrepareTarget [13203.9,8809.14,0];
_camera camPreparePos [13132.2,8810.39,3.6355];
_camera camPrepareFOV 0.7;
//setAperture 0;
//_camera setDir 90.9985;
//[_camera, -3.70588, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>A deep storyline</t>",-0.5,0.8,4,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13204.2,8808.84,0];
_camera camPreparePos [13132.2,8810.09,3.65766];
_camera camPrepareFOV 0.7;
//setAperture 0;
//_camera setDir 90.9985;
//[_camera, -3.70588, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [10437.2,7196.73,0];
_camera camPreparePos [13155.7,8866.58,4.55335];
_camera camPrepareFOV 0.51;
//setAperture 0;
//_camera setDir 238.44;
//[_camera, 10.0678, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>High performance scripting</t>",-0.5,0.15,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [10437,7196.98,0];
_camera camPreparePos [13155.6,8866.84,4.55335];
_camera camPrepareFOV 0.51;
//setAperture 0;
//_camera setDir 238.44;
//[_camera, 10.0678, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};




_camera camPrepareTarget [13718.8,9203.9,0];
_camera camPreparePos [13206.6,8883.6,15.4127];
_camera camPrepareFOV 0.65;
//setAperture 0;
//_camera setDir 57.9772;
//[_camera, -0.400514, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>For a living world</t>",0.2,0.8,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13722,9205.93,0];
_camera camPreparePos [13206.6,8883.6,15.9127];
_camera camPrepareFOV 0.65;
//setAperture 0;
//_camera setDir 57.9772;
//[_camera, -0.400514, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};




_camera camPrepareTarget [12307.4,8838.1,0];
_camera camPreparePos [13196.6,8898.28,0.700003];
_camera camPrepareFOV 0.31;
//setAperture 0;
//_camera setDir 266.13;
//[_camera, 1.90282, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>Featuring ambient civilians</t>",0.5,0.1,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [12307.4,8838.1,0];
_camera camPreparePos [13196.6,8898.28,0.700003];
_camera camPrepareFOV 0.25;
//setAperture 0;
//_camera setDir 266.13;
//[_camera, 1.90282, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};


_camera camPrepareTarget [16129.3,8338.74,0];
_camera camPreparePos [12932.4,8854.81,2.05517];
_camera camPrepareFOV 0.24;
//setAperture 0;
//_camera setDir 99.1702;
//[_camera, 2.03725, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>A bus system</t>",0.5,0.8,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [16129.3,8338.74,0];
_camera camPreparePos [12932.4,8854.81,2.05517];
_camera camPrepareFOV 0.27;
//setAperture 0;
//_camera setDir 99.1702;
//[_camera, 2.03725, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13107.3,8726.95,0];
_camera camPreparePos [13154.1,8833.51,1.55514];
_camera camPrepareFOV 0.41;
//setAperture 0;
//_camera setDir 203.728;
//[_camera, -4.41913, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>A dialogue system</t>",0.5,0.15,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13106.7,8727.22,0];
_camera camPreparePos [13153.5,8833.79,1.55514];
_camera camPrepareFOV 0.41;
//setAperture 0;
//_camera setDir 203.728;
//[_camera, -4.41913, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};




_camera camPrepareTarget [13111.5,8889.7,0];
_camera camPreparePos [13134,8905.84,3.90587];
_camera camPrepareFOV 0.57;
//setAperture 0;
//_camera setDir 234.315;
//[_camera, -8.48663, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>Freedom of choice</t>",-0.5,0.15,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13112,8889.64,0];
_camera camPreparePos [13134.3,8905.68,3.88992];
_camera camPrepareFOV 0.57;
//setAperture 0;
//_camera setDir 234.315;
//[_camera, -8.48663, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [16070.5,10070.6,0];
_camera camPreparePos [13012.2,9078.09,3.7755];
_camera camPrepareFOV 0.32;
//setAperture 0;
//_camera setDir 72.0207;
//[_camera, 7.1294, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>Side missions</t>",0.5,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [16070.5,10070.6,0];
_camera camPreparePos [13012.2,9078.09,3.7755];
_camera camPrepareFOV 0.37;
//setAperture 0;
//_camera setDir 72.0207;
//[_camera, 7.1294, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13295.8,9087.11,0];
_camera camPreparePos [13262.7,9103.31,32.6604];
_camera camPrepareFOV 0.19;
//setAperture 0;
//_camera setDir 116.071;
//[_camera, -41.5472, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'>A wide range of NPCs</t>",0.25,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13296.2,9087.92,0];
_camera camPreparePos [13263.1,9104.11,32.6604];
_camera camPrepareFOV 0.19;
//setAperture 0;
//_camera setDir 116.071;
//[_camera, -41.5472, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13161.5,8887.52,0];
_camera camPreparePos [13205.4,8935.31,20.9085];
_camera camPrepareFOV 0.18;
//setAperture 0;
//_camera setDir 222.55;
//[_camera, -15.6353, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'></t>",0.25,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13160.3,8886.21,0];
_camera camPreparePos [13205.4,8935.31,21.4085];
_camera camPrepareFOV 0.18;
//setAperture 0;
//_camera setDir 222.55;
//[_camera, -15.6353, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13272.7,9209.8,0];
_camera camPreparePos [13236.5,9160.12,2];
_camera camPrepareFOV 0.25;
//setAperture 0;
//_camera setDir 36.1146;
//[_camera, -1.86275, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'></t>",0.25,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13273.3,9209.39,0];
_camera camPreparePos [13237,9159.71,2];
_camera camPrepareFOV 0.25;
//setAperture 0;
//_camera setDir 36.1146;
//[_camera, -1.86275, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13688.8,9210.57,0];
_camera camPreparePos [13625.8,9251.6,10.2404];
_camera camPrepareFOV 0.67;
//setAperture 0;
//_camera setDir 123.08;
//[_camera, -6.17064, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'></t>",0.25,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13689.2,9210.36,0];
_camera camPreparePos [13626.8,9251,10.2404];
_camera camPrepareFOV 0.67;
//setAperture 0;
//_camera setDir 123.08;
//[_camera, -6.17064, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [11964.4,8652.38,0];
_camera camPreparePos [13409.9,8881.33,2.78499];
_camera camPrepareFOV 0.6;
//setAperture 0;
//_camera setDir 261.002;
//[_camera, 6.77993, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'></t>",0.25,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [11964.4,8651.36,0];
_camera camPreparePos [13410.1,8880.35,2.78499];
_camera camPrepareFOV 0.6;
//setAperture 0;
//_camera setDir 261.002;
//[_camera, 6.77993, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};



_camera camPrepareTarget [13706.5,9123.24,0];
_camera camPreparePos [13492.5,8990.32,1.45682];
_camera camPrepareFOV 0.45;
//setAperture 0;
//_camera setDir 58.1482;
//[_camera, 0.875366, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaLight' size = '1.5'></t>",0.25,0.75,10,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [13615.7,9018.31,0];
_camera camPreparePos [13492,8991.72,1.45682];
_camera camPrepareFOV 0.45;
//setAperture 0;
//_camera setDir 77.8677;
//[_camera, -0.359935, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 3;
waitUntil {camCommitted _camera};




player setPos _initialPos;
_camera cameraEffect ["Terminate","back"];
{deleteVehicle _x} forEach _trash;
playMusic "";
0 fadeSound 1;
setDate _oldDate;
["<t font='PuristaLight' size = '1.5'></t>",-0.5,0.15,10,0.5,0] spawn tov_fnc_dynamicText; 





/*

Custom AI
Branching plot
Hookers
FM radios

Take the role
Of an FSB operative
In the Kingdom of Sahrani
*/






/*
_playing=["videos\intro1.ogv"] spawn BIS_fnc_playVideo;
sleep 122;
"background" cutText ["", "BLACK FADED", 30, true, true];
waitUntil {scriptDone _playing;};
*/
/*
//8 years later
sleep 2;
"ctext" cutText ["<t font='PuristaMedium' size='5'>8 years later</t>", "PLAIN", -1, true, true];
sleep 3;
"ctext" cutFadeOut  2;
sleep 3;

//News thingy
_playing=["videos\intro2.ogv"] spawn BIS_fnc_playVideo;
"background" cutText ["", "BLACK IN", 3, true, true];
sleep 19;
"background" cutText ["", "BLACK FADED", 30, true, true];
waitUntil {scriptDone _playing;};
_playing=["videos\static.ogv"] spawn BIS_fnc_playVideo;
waitUntil {scriptDone _playing;};

_camera camPrepareTarget [-46139.91,84479.46,-20282.28];
_camera camPreparePos [7118.16,2089.99,130.83];
_camera camPrepareFOV 0.432;
_camera camCommitPrepared 120;
"background" cutText ["", "BLACK IN", 3, true, true];

sleep 5;
[""] spawn BIS_fnc_playVideo;
"ctext" cutText ["<t font='PuristaMedium' shadow='2' size='5'>Chernogorsk</t>", "PLAIN", -1, true, true];
sleep 3;
"ctext1" cutText ["<t font='PuristaMedium' shadow='2' size='3'><br/><br/><br/><br/>South Zagoria</t>", "PLAIN", -1, true, true];
sleep 3;

if (isServer) then {"Bo_GBU12_LGB" createVehicle [6733.44,2441.95,0];};

sleep 6;
"background" cutText ["", "BLACK", 2, true, true];
sleep 4;
"ctext" cutText ["", "PLAIN", 0.0001, true, true];
"ctext1" cutText ["", "PLAIN", 0.0001, true, true];


_camera cameraEffect ["Terminate","back"];
{_x cutText ["", "PLAIN", 0.0001, true, true];} forEach allCutLayers;
"background" cutText ["", "BLACK IN", 3, true, true];
sleep 1;
["videos\news.ogv","newsSound",tv] spawn playVideo;
sleep 2;
*/