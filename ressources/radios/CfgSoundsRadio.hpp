	class EspantosoFM
	{
		// how the sound is referred to in the editor (e.g. trigger effects)
		name = "Espantoso FM";

		// filename, volume, pitch, distance (optional)
		sound[] = { "\ressources\radios\EspantosoFM.ogg", 1, 1};

		// subtitle delay in seconds, subtitle text
		titles[] = { 0, "" };
	};

	class EastLosFM
	{
		// display name
		name	= "East Los FM";
		// Lenght = 2465 
		// filename, volume, pitch
		sound[]	= { "\ressources\radios\EastLosFM.ogg", db + 0, 1.0 };
		titles[] = { 0, "" };
		
	};
	
	class SanJuanRadio
	{
		// display name
		name	= "San Juan Radio";
		// Lenght = 3698 
		// filename, volume, pitch
		sound[]	= { "\ressources\radios\SanJuanRadio.ogg", db + 0, 1.0 };
		titles[] = { 0, "" };
	};
		
	class Emotion
	{
		// display name
		name	= "Emotion 98.3";
		// Lenght = 3545  
		// filename, volume, pitch
		sound[]	= { "\ressources\radios\Emotion.ogg", db + 0, 1.0 };
		titles[] = { 0, "" };
	};
		